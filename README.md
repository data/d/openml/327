# OpenML dataset: bridges

https://www.openml.org/d/327

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Yoram Reich","Steven J. Fenves  
  
**Source**: [original](http://openml.org/d/17) -   
**Please cite**:   

Pittsburgh bridges  

This version is derived from version 1 by removing all instances with missing values in the last (target) attribute. The bridges dataset is originally not a classification dataset, put is used so extensively in the literature, using the last attribute as the target attribute. However, this attribute has missing values, which may lead to confusing benchmarking result. Therefore, these instances have been removed. 

Sources: 
-- Yoram Reich and Steven J. Fenves Department of Civil Engineering and Engineering Design Research Center Carnegie Mellon University Pittsburgh, PA 15213  Compiled from various sources.  
-- Date: 1 August 1990  

Attribute Information:   The type field state whether a property is continuous/integer (c)  or nominal (n). For properties with c,n type, the range of continuous numbers is given  first and the possible values of the nominal follow the semi-colon.   

name type possible values comments 
------------------------------------------------------------------------ 
1. IDENTIF - - identifier of the examples 
2. RIVER n A, M, O 
3. LOCATION n 1 to 52 
4. ERECTED c,n 1818-1986 -  CRAFTS, EMERGING, MATURE, MODERN 
5. PURPOSE n WALK, AQUEDUCT, RR, HIGHWAY 
6. LENGTH c,n 804-4558 - SHORT, MEDIUM, LONG 
7. LANES c,n 1, 2, 4, 6 - 1, 2, 4, 6 
8. CLEAR-G n N, G 
9. T-OR-D n THROUGH, DECK 
10. MATERIAL n WOOD, IRON, STEEL 
11. SPAN n SHORT, MEDIUM, LONG 
12. REL-L n S, S-F, F 
13. TYPE n WOOD, SUSPEN, SIMPLE-T, ARCH, CANTILEV, CONT-T

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/327) of an [OpenML dataset](https://www.openml.org/d/327). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/327/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/327/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/327/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

